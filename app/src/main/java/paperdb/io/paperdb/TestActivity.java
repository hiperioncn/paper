package paperdb.io.paperdb;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.paperdb.Paper;

public class TestActivity extends AppCompatActivity {
    Button buttonTest;
    //The bean of person is too simply, so set large size.
    List<Person> insertedPersons = TestDataGenerator.genPersonList(100000);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        buttonTest =(Button)findViewById(R.id.button_read);
        buttonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testReadSlow();
            }
        });
        Paper.init(this);
        Paper.book().write("key1","ABCDEFGHIJKLMNOPQRSTUVWXYZ0");
    }

    public void testReadSlow()  {
        ExecutorService mExecutor = Executors.newFixedThreadPool(10);
//        mExecutor.submit(getInsertPersons());
//        mExecutor.submit(getSelectKey1());
        mExecutor.submit(getSelectPersons());
        mExecutor.submit(getSelectKey1());
        mExecutor.submit(getSelectPersons());

    }

    private Runnable getInsertPersons() {
        return new Runnable() {
            @Override
            public void run() {
                long s1 = System.currentTimeMillis();
                Log.d(">>>>>>>", "write persons start");
                Paper.book().write("persons", insertedPersons);
                long s2 = System.currentTimeMillis();
                Log.d(">>>>>>>", "write persons end spend:" + (s2 - s1) + "ms");
            }
        };
    }

    private Runnable getSelectKey1() {
        return new Runnable() {
            @Override
            public void run() {
                long s1 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read key1 start");
                Paper.book().read("key1");
                long s2 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read key1 end spend:" + (s2 - s1) + "ms");
            }
        };
    }
    private Runnable getSelectPersons() {
        return new Runnable() {
            @Override
            public void run() {
                long s1 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read persons start");
                Paper.book().read("persons");
                long s2 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read persons end spend:" + (s2 - s1) + "ms");
            }
        };
    }
}
