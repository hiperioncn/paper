package io.paperdb.multithread;

import android.test.AndroidTestCase;
import android.util.Log;

import org.junit.Before;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import io.paperdb.Paper;
import io.paperdb.testdata.Person;
import io.paperdb.testdata.TestDataGenerator;

import static android.support.test.InstrumentationRegistry.getTargetContext;

/**
 * Tests read/write into Paper data from multiple threads
 */
public class MultiThreadTest extends AndroidTestCase {
    @Before
    public void setUp() throws Exception {
        Paper.clear(getTargetContext());
        Paper.init(getTargetContext());
    }
    public void testMultiThreadAccess() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        List<Callable<Object>> todo = new LinkedList<>();

        for (int i = 0; i <= 10; i++) {
            Runnable task;
            if (i % 2 == 0) {
                task = getInsertRunnable();
            } else {
                task = getSelectRunnable();
            }
            todo.add(Executors.callable(task));
        }
        List<Future<Object>> futures = executor.invokeAll(todo);
        for (Future<Object> future : futures) {
            future.get();
        }
    }

    private Runnable getInsertRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                int size = new Random().nextInt(20000);
                final List<Person> inserted100 = TestDataGenerator.genPersonList(size);
                long s2 = System.currentTimeMillis();
                Paper.book().write("persons", inserted100);
                long s3 = System.currentTimeMillis();
                if(s3-s2>1000) {
                    Log.d(">>>>>>>write", "persons size" + size + " spend:" + (s3 - s2) + "ms");
                }
            }
        };
    }

    private Runnable getSelectRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                long s2 = System.currentTimeMillis();
                Paper.book().read("persons");
                long s3 = System.currentTimeMillis();
                if(s3-s2>1000) {
                    Log.d(">>>>>>>read", "persons spend:" + (s3 - s2) + "ms");
                }
            }
        };
    }
}
