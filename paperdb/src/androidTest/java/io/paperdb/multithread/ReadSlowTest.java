package io.paperdb.multithread;

import android.test.AndroidTestCase;
import android.util.Log;

import org.junit.Before;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.paperdb.Paper;
import io.paperdb.testdata.Person;
import io.paperdb.testdata.TestDataGenerator;

import static android.support.test.InstrumentationRegistry.getTargetContext;

/**
 * Tests read slow into Paper data from multiple threads
 */
public class ReadSlowTest extends AndroidTestCase {
    boolean finished = false;
    boolean finished1 = false;
    int count = 2;
    //The bean of person is too simply, so set large size.
    List<Person> insertedPersons = TestDataGenerator.genPersonList(100000);
    @Before
    public void setUp() throws Exception {
        Paper.clear(getTargetContext());
        Paper.init(getTargetContext());
//        Paper.book().write("key1","ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
    }

    public void testReadSlow() throws Exception {
        ExecutorService mExecutor = Executors.newFixedThreadPool(10);
        mExecutor.submit(getInsertPersons());
//        mExecutor.submit(getSelectPersons());
        mExecutor.submit(getSelectKey1());


        while (count!=0){
            //wait for test finish
        }
        Log.d(">>>>>>","Finish");
    }

    private Runnable getInsertPersons() {
        return new Runnable() {
            @Override
            public void run() {
                long s1 = System.currentTimeMillis();
                Log.d(">>>>>>>", "write persons start");
                Paper.book().write("persons", insertedPersons);
                long s2 = System.currentTimeMillis();
                Log.d(">>>>>>>", "write persons end spend:" + (s2 - s1) + "ms");
                finished = true;
                count--;
            }
        };
    }

    private Runnable getSelectKey1() {
        return new Runnable() {
            @Override
            public void run() {
                long s1 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read key1 start");
                Paper.book().read("key1");
                long s2 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read key1 end spend:" + (s2 - s1) + "ms");
                finished1 = true;
                count--;
            }
        };
    }
    private Runnable getSelectPersons() {
        return new Runnable() {
            @Override
            public void run() {
                long s1 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read persons start");
                Paper.book().read("persons");
                long s2 = System.currentTimeMillis();
                Log.d(">>>>>>>", "read persons end spend:" + (s2 - s1) + "ms");
                count--;
                finished1 = true;
            }
        };
    }
}
