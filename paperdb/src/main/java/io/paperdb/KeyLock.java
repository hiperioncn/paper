package io.paperdb;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;

/**
 * Created by hiperion on 2017/3/13.
 */

public class KeyLock<K> {
    // 保存所有锁定的KEY及其信号量
    private final ConcurrentMap<K, Semaphore> map = new ConcurrentHashMap<K, Semaphore>();
    // 保存每个线程锁定的KEY及其锁定计数
    private final ThreadLocal<Map<K, LockInfo>> local = new ThreadLocal<Map<K, LockInfo>>() {
        @Override
        protected Map<K, LockInfo> initialValue() {
            return new HashMap<K, LockInfo>();
        }
    };

    public void lock(K key) {
        if (key == null)
            return;
        Semaphore previous = map.get(key);
        if (previous == null) {
            System.out.println(">>>>>>>没有lock key: "+ key+" 的线程");
            Semaphore current = new Semaphore(1);
            current.acquireUninterruptibly();
            map.put(key, current);
        } else {
            System.out.println(">>>>>>>有lock key: "+ key+" 的线程");
            previous.acquireUninterruptibly();
        }
    }
    /**
     * 释放key，唤醒其他等待此key的线程
     * @param key
     */
    public void unlock(K key) {
        if (key == null)
            return;
        Semaphore previous = map.get(key);
        if (previous != null) {
            System.out.println(">>>>>>>释放lock key: "+ key+" 的线程");
            previous.release();
            map.remove(key);
        } else {
            System.out.println(">>>>>>>key: "+ key+" LockInfo is null?");
        }
    }
    /**
     * 锁定key，其他等待此key的线程将进入等待，直到调用{@link #unlock(K)}
     * 使用hashcode和equals来判断key是否相同，因此key必须实现{@link #hashCode()}和
     * {@link #equals(Object)}方法
     *
     * @param key
     */
    public void lock1(K key) {
        if (key == null)
            return;
        LockInfo info = local.get().get(key);
        if (info == null) {
            System.out.println(">>>>>>>没有lock key: "+ key+" 的线程");
            Semaphore current = new Semaphore(1);
            current.acquireUninterruptibly();
            Semaphore previous = map.put(key, current);
            if (previous != null)
                previous.acquireUninterruptibly();
            local.get().put(key, new LockInfo(current));
        } else {
            System.out.println(">>>>>>>有lock key: "+ key+" 的线程,前面还有"+info.lockCount+"个线程");
            info.lockCount++;
        }
    }

    /**
     * 释放key，唤醒其他等待此key的线程
     * @param key
     */
    public void unlock1(K key) {
        if (key == null)
            return;
        LockInfo info = local.get().get(key);
        if (info != null && --info.lockCount == 0) {
            System.out.println(">>>>>>>释放lock key: "+ key+" 的线程");
            info.current.release();
            map.remove(key, info.current);
            local.get().remove(key);
        } else {
            System.out.println(">>>>>>>key: "+ key+" LockInfo is null?"+(info==null)+ " lockCount:"+info.lockCount);
        }
    }

    /**
     * 锁定多个key
     * 建议在调用此方法前先对keys进行排序，使用相同的锁定顺序，防止死锁发生
     * @param keys
     */
    public void lock(K[] keys) {
        if (keys == null)
            return;
        for (K key : keys) {
            lock(key);
        }
    }

    /**
     * 释放多个key
     * @param keys
     */
    public void unlock(K[] keys) {
        if (keys == null)
            return;
        for (K key : keys) {
            unlock(key);
        }
    }

    private static class LockInfo {
        private final Semaphore current;
        private int lockCount;

        private LockInfo(Semaphore current) {
            this.current = current;
            this.lockCount = 1;
        }
    }
}
